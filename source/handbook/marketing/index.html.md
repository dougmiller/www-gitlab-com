---
layout: markdown_page
title: "Marketing"
---
# Welcome to the GitLab Marketing Handbook  
The GitLab Marketing team includes Demand Generation, Developer Relations, Design, and Product Marketing

[Up one level to the GitLab Handbook](/handbook/)    

## On this page
* [Marketing mission](#mission)
* [Functional groups](#groups)
* [Marketing OKRs](#okrs)
* [Meetings and structure](#meetings)
* [Marketing team SLAs](#sla)
* [Resources](#resources)
* [Slack marketing channels](#chat)  
* [Marketing handbook updates](#handbook)
* [Marketing emails](#email)

## Marketing Handbooks
* [Demand Generation](/handbook/marketing/demand-generation)
* [Developer Relations](/handbook/marketing/developer-relations)
* [Design](/handbook/marketing/design/)
* [Product Marketing](/handbook/marketing/product-marketing)

# GitLab Marketing Mission: GitLab for All!<a name="mission"></a>

We think GitLab (.com, CE, and EE) can help developers, designers, IT workers, marketers, and everyone in between improve collaboration.

Our marketing issues are publicly viewable via the [GitLab Marketing issue tracker](https://gitlab.com/gitlab-com/marketing/issues).

The GitLab marketing team is composed of a unique set of individuals from interesting backgrounds all over the world. Because of our unique skill sets, we're able to accomplish a lot of high impact campaigns through collaboration and open feedback from our team and the community.  

# Marketing Team Functional Groups<a name="groups"></a>

Our Marketing team is split into four key functional groups. All four groups are important for the success of GitLab.

### Demand generation

- **Demand Generation:** [Handbook](/handbook/marketing/demand-generation), [Job Description](https://about.gitlab.com/jobs/demand-generation-manager/)  
- **Online Marketing:** [Handbook](/handbook/marketing/demand-generation/online-marketing/), [Job Description](https://about.gitlab.com/jobs/online-marketing-manager/)
- **Business Development:** [Handbook](/handbook/marketing/demand-generation/business-development/), [Job Description](/jobs/business-development-representative/)

### Design

- **Design:** [Handbook](/handbook/marketing/design), [Job Description](/jobs/designer/)

### Developer Relations

- **Technical Writing:** [Handbook](/handbook/marketing/developer-relations/technical-writing/), [Job Description](/jobs/technical-writer/)
- **Developer Advocacy:** [Handbook](/handbook/marketing/developer-relations/developer-advocacy/), [Job Description](/jobs/developer-advocate)  
- **Field Marketing:** [Handbook](/handbook/marketing/developer-relations/field-marketing/), [Job Description](/jobs/field-marketing-manager)

### Product Marketing

- **Product Marketing:** [Handbook](/handbook/marketing/product-marketing/), [Job Description](/jobs/product-marketing-manager)
- **Partner Marketing:** [Handbook](/handbook/marketing/product-marketing/#partnermarketing/), Job Description coming soon.
- **Content Marketing:** [Handbook](/handbook/marketing/developer-relations/content-marketing/), [Job Description](/jobs/content-marketing-manager)

# Marketing OKRs<a name="okrs"></a>

Our team and the demands on marketing are growing quickly. In order to align our goals with company goals as well as prioritize what we are working on, OKRs help us to maintain structure. Of course, not everything can be captured in one Google Sheet but this helps us all to know what we consider our goals as a team to be.  

Each member of the marketing team is responsible for 3 Objectives and 3 Key Results for each objective.  

To find the GitLab Marketing Team's OKRs, search the company Google Drive for "Marketing OKRs - Quarterly".

- What is an OKR? Objectives and Key Results
- Objectives are goals
- Key Results are measurement
- OKRS help to prioritize and align
- OKRs are set quarterly
- OKRs are NOT a measurement of performance.
- Set goals that aren’t easy to hit.
- Graded on a scale of 0.0 to 1.0. Normal is around .8
- [Quick overview of OKRs](http://www.slideshare.net/HenrikJanVanderPol/how-to-outperform-anyone-else-introduction-to-okr)

# Meetings and structure<a name="meetings"></a>  

These are just the required meetings for team members and managers. Of course, meetings are encouraged when it expedites a project or problem solving amongst member so the team and company. Don't be afraid to say "Hey, can we hangout?" if you need help with something.

### Weekly 1:1 (New hire: First month with all direct reports)

**Meeting goal: For manager to help new team member onboard quickly.**  

**Run time: 30 minutes**

All managers should have a weekly 1:1 with their direct reports in the first month of employment, starting with the first day of employment where possible.  

The first meeting should run as follows:  

- Welcome and make sure new hire is working through onboarding tasks.  
- Suggest people on the marketing team and beyond to meet with.  
- Manager should make sure all technology needs are taken care of.  
- Manager should answer or help to find proper resource for any questions.  
- Manager should create Google Doc private 1:1 agenda for all recurring 1:1's and add to the description of the calendar invite.  

The agenda of the following 1:1s should be the same as the recurring Bi-weekly 1:1s with time set aside to answer any questions about onboarding.


### Bi-weekly 1:1 (After first month with all direct reports)

**Meeting goal: For manager to remove roadblocks and help prioritize so team member can be effective.**  

**Run time: 30 minutes**

All managers should have twice monthly (bi-weekly) meetings with all of his or her direct reports.

The meeting should run as follows:  

- Always add agenda items to Google Doc agenda in description of meeting invite.   
- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?    
- Go through the agenda and answer any questions.    
- Action items for the employee should be marked in Red.    
- Action items for the manager should be marked in Blue.    
- All action items should be handled before next 1:1 unless otherwise noted.    

### Every 6 weeks 1:1 (All members of marketing team with executive management)

**Meeting goal: For CMO to help with any questions and career path discussion.**  

**Run time: 30 minutes**

All members of the marketing team that are not direct reports should meet with their executive management (CMO) once every 6 weeks. If questions or concerns arise, please don't hesitate to reach out directly for an impromptu discuss via email or slack.

The meeting should run as follows:  

- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?  
- Talk about career development and opportunities for growth.  
- Walk through quarterly OKRs to make sure they are relevant and see if help is needed.  
- Discuss upcoming OKRs if close to end of a quarter.  

### Monthly Marketing Townhall

**Meeting goal: For the entire company to gain insight into what the Marketing team is focusing on.**

**Run time: 45 minutes**

The Monthly Marketing Townhall is open to the entire company for questions, ideas, and feedback. If you'd like to be added to this meeting, please contact anyone on the marketing team to be added to the invite.

To find the agenda for the bi-weekly marketing meeting, search the company Google Drive folder for "Monthly Marketing Townhall Agenda"

The meeting should run as follows:  

- 24 hours prior to the meeting all marketing members should have their sections of the agenda completed.  
- The CMO will email all attendees reminding them to take a look at the agenda and add any questions to the top of the meeting.  
- Meeting will open with the CMO giving an overview of new hires, organization changes, large upcoming campaigns, etc.  
- The meeting will then open to questions starting with the questions already documented.
- Everyone is encouraged to participate.    

### Tuesday & Thursday dinosaur party   

**Meeting goal: Dinosaur Party! (and also make sure we're all working together effectively)**    
**Run time: 10-15 minutes**  

The Tuesday and Thursday Dinosaur Party is where the entire team meets for 10-15 minutes immediately following the team call to discuss what we're all working on and to get help with any questions. There is no agenda for this meeting and it should be an open forum.  

The meeting should run as follows:  

- First 60 seconds: Post funny GIFs in Slack marketing room and person with funniest gets a dinosaur sticker. Person with most dinosaur stickers per quarter gets $250 bonus. Voting occurs with Smiley face emojis  
- First 5 minutes: Everyone in order of Team Page (Last goes first) gives 15 seconds about what they are working on. (Marcia, Colton, Amara, Ryan, Amanda, Braden, Mitchell, Ivan, Luke, Axil, Hank, Ashley, Emily)  
- Last 5 minutes: Discuss roadblocks and any collaboration needs  

To find the Dinosaur Sticker leaderboard, search the company Google Drive for "Dinosaurs are awesome"

### Monthly Marketing Team Meeting

**Meeting goal: OKR review and large campaign planning**

**Run time: 30 minutes**

The Monthly Marketing Team Meeting is for review of our team OKRs (which we set at the Quarterly Marketing Review) and to discuss any large campaigns that are happening in the upcoming month.

The agenda for the meeting can be found by searching the company Google Drive for "Monthly Marketing Meeting"

The meeting should run as follows:

- Agenda items should be addressed first  
- Discuss any large upcoming campaigns  
- Team OKRs are quickly reviewed and graded   

### Quarterly marketing review

**Meeting goal: How did the last quarter go? What should we do for the upcoming quarter?**

**Run time: 120 minutes**

The Quarterly Marketing Review takes the place of the Monthly Marketing Meeting every last month of the new Quarter.

CMO works with the functional group leads to review planning for each group.

The meeting should run as follows:

- CMO leads review of over marketing strategies of last quarter. What worked? What didn't work? What should we do more of?  
- Head of Product Marketing reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Developer Relations reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Demand Generation reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Design reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Team agrees on what key takeaways are from the last quarter.  
- Team agrees on what our Team OKRs should be for the upcoming quarter.  

# Marketing team SLAs (Service Level Agreements)<a name="sla"></a>

When working remotely in such a quick moving organization, it is important for a team to agree on a few basic service level agreements on how we would like to work together. With anything, things can come up that make it not possible to meet these SLAs but we all agree to use best effort when possible.    

- Respond to your emails by end of next business day.
- Respond when you are cc'd with an action item on issues by end of next business day.  
- Be on time to meetings. We start at on time.  
- Acknowledge receipt of emails (community@, FYIs) by BCC'ing the list.
- Try not to email co-workers on weekends. Try out [Boomerang](http://www.boomeranggmail.com/) and set all your emails to send Monday morning at 6 AM. People will think you're up and working early! Time off is important. We all have stressful weeks so please unplug on the weekends where possible.
- Do not ping someone in a public channel on Slack on the weekends. This is rude.

# Slack marketing channels<a name="chat"></a>

We use Slack internally as a communication tool. The marketing channels are as follows:  

- Marketing - This is the general marketing channel. Don't know where to ask a question? Start here.
- Advertising - Online marketing channel
- BDR team - For the BDR team
- Blog - Questions about the blog? This is your place.
- CFP - All call for speakers will be posted here.
- DevRel - A channel for the developer relations team to collaborate.
- Docs - Technical writing and documentation questions? This is your room.
- Events - Everything you want to know about events.
- Marketo Users - Having issues with Marketo? Ask here first.
- Newsletters - Watch our twice monthly newsletter get made here.
- Social - Twitter, Facebook, and other social media questions?
- Webcasts - Watch the webcast behind the scenes.  

# Marketing Handbook Updates<a name="handbook"></a>

Anything that is a process in marketing should be documented in the Marketing Handbook.  

- Format of all pages should be as follows:  
    - Welcome to the Handbook
    - Functional group overview if handbook for entire functional group or organization
    - Link up one level
    - "On this page" index of all top level headers on the current page
    - Links to other handbooks included on this page.
- Rather than create many nested pages, include everything on one page of your role's handbook with an index at the top.
- Each role should have a handbook page.
- If more than one person are performing a role, the task should be shared to update the handbook with all process or guidelines.
- Follow the style guide set by this marketing handbook page.

# Marketing email alias list<a name="email"></a>

- Community@gitlab.com is an external email address that goes to the CMO, Field Marketing Manager, Senior Product Marketing Manager, and Demand Generation team.  
- Marketing@gitlab.com is an internal team email address for everyone on the marketing team.  
- Press@gitlab.com directs to the CMO and Senior Product Marketing Manager.  
- News@gitlab.com is an external email address for sending newsletters that goes to the Online Marketing Manager and Senior Demand Generation Manager.  
- Securityalerts@gitlab.com is an external email address for sending security alerts that goes to the Online Marketing Manager and Senior Demand Generation Manager.  
